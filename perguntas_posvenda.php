<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="include/style/formcontrol.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Include the above in your HEAD tag ---------->
  <title>Perguntas Pós Venda - Mercado Livre</title>
</head>
<body>
  <?php
  ini_set("error_reporting",E_ALL);
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  require 'include/all_include.php';
  echo "<pre>"; //
  $wcmeliOrder = new wcmeliOrder;
  $fluxOrderQuestionAfterSale = new flux('wcmeli_product_questionAfterSale');
  $fluxOrderQuestionAfterSale->pathListItem = true;
  if(!$fluxOrderQuestionAfterSale->setFiles()) {
    $questionsAfterSale = $wcmeliOrder->meliGetQuestionsId();
    $fluxOrderQuestionAfterSale->list_item = $questionsAfterSale;
    $fluxOrderQuestionAfterSale->setFiles();
  }
  $fluxOrderQuestionAfterSale->getFiles();
  $orderId = $fluxOrderQuestionAfterSale->list_item[0];
  if(!$orderId) $error = true;

  // $id = $class->retornaPerguntasNaoLidas($order_id);
  if(!isset($error)) {
    // $order_id = [];
    // foreach ($id->results as $key => $value) {
    //   $order_id = $value->order_id;
    // }
    // var_dump(file_put_contents("$class->list_message",$order_id));
    $orderInfo = $wcmeliOrder->meliGetOrder($order_id)['body'];
    if($orderInfo->pack_id != '' || !empty($orderInfo->pack_id)) $order_id = $orderInfo->pack_id;
    $dados = $wcmeliOrder->meliGetQuestionsByOrderId($order_id)['body']->results;

  ?>
  <div class="container contact-form">
    <div class="contact-image">
      <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
  <form method="get" action="perguntas_posvenda_be.php">
      <div class="">
      <h3>Perguntas Pós Venda - Mercado Livre </h3>
      <?php if (isset($_GET['sucesso'])){ echo '<div class="sucesso" style="color:green;">Mensagem respondida com sucesso</div>';}
      if (isset($_GET['problema'])){ echo '<div class="problema" style="color:red;">Mensagem não pôde ser respondida</div>';}?>
            <?php
              // var_dump($dados);

              foreach ($dados as $key => $value) {
                // var_dump($dados);
                echo '<div style="border: 1px solid #1d5aea;padding:5px;">';
                if($value['from'] == 'NFe') {$value['from'] = "Instrumentos Cirurgicos Prisci";
                echo '<label>Quem enviou: '.$value['from'].'</label><br>';
                echo '<label >Assunto: '.$value['subject'].'</label><br>';
                echo '<label  name="pergunta" value="">Data:'.date("d-m-y H:m:s",strtotime($value->date_received)).'</label><br>';
              // echo '<label><b>Mensagem</b></label><br>';
              echo '<textarea  type="text" name="pergunta" cols="70" rows="5" readyonly="true" value="">'.$value->text->plain.'</textarea><br><br>';
              echo '</div><br>';
            } else {
              echo '<input name="from" hidden="true" value="'.$value['from'].'">Quem enviou: '.$value->from->name.'</input><br>';
              echo '<input name="user_id" hidden="true" value="'.$value['user_id'].'">Id Cliente: '.$value->from->user_id.'</input><br>';
              echo '<input name="subject" hidden="true" value="'.$value['subject'].'">Assunto: '.$value->subject.'</label><br>';
              echo '<label  name="pergunta" value="">Data:'.date("d-m-y H:m:s",strtotime($value->date_received)).'</label><br>';
            // echo '<label><b>Mensagem</b></label><br>';
            echo '<textarea  type="text" name="pergunta" cols="70" rows="5" readyonly="true" value="">'.$value->text->plain.'</textarea><br><br>';
            echo '</div><br>';
            }
            }
            ?>

              <?php echo '<input id="id" name="id" hidden="true" value="'.$order_id.'"/>';?>
          <div class="radio" class="col-md-4">
            <label><b>Resposta</b></label>
            <textarea type="text" name="txtarea" value="resposta" cols="75" rows="5"></textarea><br><br>

          </div>
          <?php if(isset($_GET['erro'])) echo '<div class="erro" style="color:red;">*Favor digitar a resposta </div><br>'; ?>
            <div class="form-group">
              <input type="submit" name="btnSubmit" class="btnContact" value="Responder" />
            </div>
                </div>
      </form>
  </div>
  <?php
 } else{
     echo '<div class="container contact-form">
    <div class="contact-image">
      <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
    <form method="get" action="perguntas_respostas_be.php">
      <h3>Perguntas Pós Venda - Mercado Livre </h3>';
      if (isset($_GET['sucesso'])){ echo '<div class="sucesso" style="color:green;">Mensagem respondida com sucesso</div>';}
      if (isset($_GET['problema'])){ echo '<div class="problema" style="color:red;">Mensagem não pôde ser respondida</div>';}
      echo '<div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <h2>Perguntas</h2>
            <label type="text" name="pergunta" class="form-control" readyonly="true" value="">Nenhuma nova pergunta</label>
          </div>
          </div>

        </div>
      </form>
    </div>';//file_put_contents($class->last_message,$order_id);
  }?>
</body>
