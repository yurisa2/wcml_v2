<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="include/style/formcontrol.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Include the above in your HEAD tag ---------->
  <title>Criação de Produtos - Mercado Livre</title>
</head>
<body>
  <div class="container contact-form">
    <div class="contact-image">
      <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
    <form method="post" action="createProductBe.php">
      <h3>Criação de Produtos - Mercado Livre </h3>
      <?php
      define( 'WP_DEBUG', true );
      echo '<pre>';
      require(__DIR__.'/../../../../wp-load.php');
      include_once 'include/all_include.php';


      $args = array(
          'posts_per_page' => 100 // Important to know there is a pagination going on here, need to decide what to do with it
        );

        $query = new WC_Product_Query($args);
        $allProducts = $query->get_products();



      $wcmeliProduct = new meliProduct;

      $fluxProduct = new flux('wcmeli_createproduct');
      $fluxProduct->timeFile = true;
      $fluxProduct->pathListItem = true;
      $fluxProduct->nfeFile = false;
      $fluxProduct->storeOrderList = false;
      $fluxProduct->pagination = true;
      // create a list of woocommerce products
      if(!$fluxProduct->setFiles()) {
        $listProductId = $allProducts;
        $fluxProduct->list_item = $listProductId;
        $fluxProduct->setFiles();
        echo '<h3>Lista de Produtos do WooCommerce Pronta.</h3>';
      }
      $fluxProduct->getFiles();
      $fluxProduct->addCounter();
      $productSkuAndId = (array)$fluxProduct->test_next_item();


      if(!file_exists('include/files/wcmeli_createdproducts_LIST.json')) file_put_contents('include/files/wcmeli_createdproducts_LIST.json',json_encode(array()));

      if(empty(json_decode(file_get_contents('include/files/wcmeli_createdproducts_LIST.json')))) {
        $productsSku = $allProducts;
        file_put_contents('include/files/wcmeli_createdproducts_LIST.json',json_encode($productsSku));
        echo '<h3>Lista de Produtos Já Criado no Mercado Livre Pronta.</h3>';
      }

      $meliProductsSku = json_decode(file_get_contents('include/files/wcmeli_createdproducts_LIST.json'));



      if(in_array($productSkuAndId['sku'],$meliProductsSku)) {
      // if(1 == 2) {
        if (isset($_GET['sucesso'])){ echo '<div class="sucesso" style="color:green;">Produto Criado com sucesso</div>';}
        if (isset($_GET['problema'])){ echo '<div class="problema" style="color:red;">Produto não pôde ser criado</div>';}
        echo '<div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label type="text" name="pergunta" class="form-control" readyonly="true" value="">Produto [ID]'.$productSkuAndId['id'] .' - [SKU]'.$productSkuAndId['sku'] .' já foi criado</label>
        </div>
        </div>

        </div>
        </form>
        </div>';
      } else {

      // $wcProduct = $wcmeliProduct->wooCommerceProduct->wooCommerceGetProduct($productSkuAndId['id'])->product;
      // $query->set( 'id', '34' );
      // $wcProduct = $query->get_product();
      $product = wc_get_product( 34 );

      // var_dump($product->get_image());
      // exit('23');

      $description = str_replace('</div>','',substr($product->get_description(),strpos($product->get_description(),'<p>')));

      echo '<label><b>Id Produto WC: 34</b></label><br>';
      // echo '<label><b>SKU Produto: '.$wcProduct->sku.'</b></label><br>';
      // var_dump($wcProduct);

      if (isset($_GET['sucesso'])){ echo '<div class="sucesso" style="color:green;">Produto Criado com sucesso</div>';}
      if (isset($_GET['problema'])){ echo '<div class="problema" style="color:red;">Produto não pôde ser criado</div>';}?>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <?php
              $meliProduct = new meliProduct;

              $body = [['title' => $product->get_title()]];
              $params = array(
                'access_token' => $meliProduct->accessToken,
              );

              $response = $wcmeliProduct->post("/sites/MLB/category_predictor/predict",$body, $params);
              $category = end($response['body'][0]->path_from_root);

              echo '<label type="text" class="form-control" readyonly="true" value="">Nome da Categoria <b>'.$category->name.'</b></label>';
              echo '<label type="text" class="form-control" readyonly="true" value="">ID da categoria <b>'.$category->id.'</b></label><br><br>';
              echo '<label><b>Id Produto WC</b></label><br><input size="20" type="text" name="id" value="34"/><br>';
              echo '<label><b>Nome Produto</b></label><br><input size="20" type="text" name="titulo" value="'.$product->get_title().'"/><br>';
              echo '<label><b>Id Categoria</b></label><br><input size="20" type="text" name="categoryid" value="'.$category->id.'"/><br>';
              echo '<label><b>Preço Produto</b></label><br><input size="20" type="text" name="price" value="'.$product->get_price().'"/><br>';
              echo '<label><b>Estoque Produto</b></label><br><input size="20" type="text" name="stock" value="1"/><br>';
              echo '<label><b>Descrição Produto</b></label><br><input size="50" height="10" type="text" name="description" value="'.$description.'"./><br>';
              echo '<label><b>Url Imagem</b></label><br><input size="20" type="text" name="images" value="http://localhost/wcsa2meli/wordpress/wp-content/uploads/2019/01/pennant-1-300x300.jpg"/><br>';
              echo '<label><b>Sku Produto</b></label><br><input size="20" type="text" name="sku" value="'.$product->get_sku().'"/><br>';
              ?>

              <?php if(isset($_GET['erro'])) echo '<div class="erro" style="color:red;">*Favor preencher todos os dados</div><br>'; ?>
                <div class="form-group">
                  <input type="submit" name="btnSubmit" class="btnContact" value="Enviar" />
                </div>
              </div>

            </div>
          </form>
        </div>
      <?php
    }
            echo time()-$time;?>
      </body>
