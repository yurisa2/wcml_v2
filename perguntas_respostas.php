<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="include/style/formcontrol.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Include the above in your HEAD tag ---------->
  <title>Perguntas e Respostas - Mercado Livre</title>
</head>
<body>
  <?php
  ini_set("error_reporting",E_ALL);
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  require 'include/all_include.php';
  echo "<pre>";
  $wcmeliProduct = new wcmeliProduct;
  $fluxProductQuestion = new flux('wcmeli_product_question');
  $fluxProductQuestion->pathListItem = true;
  if(!$fluxProductQuestion->setFiles()) {
    $questionIds = $wcmeliProduct->meliGetQuestionIds();
    $fluxProductQuestion->list_item = $questionIds;
    $fluxProductQuestion->setFiles();
  }
  $fluxProductQuestion->getFiles();
  $productId = $fluxProductQuestion->next_item();

  if($productId != false) {

  $pergunta = $wcmeliProduct->meliGetQuestion($productId)['body'];


  $response = $wcmeliProduct->meliGetProduct($pergunta->item_id)['body'];

  ?>
  <div class="container contact-form">
    <div class="contact-image">
      <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
    <form method="get" action="perguntas_respostas_be.php">
      <h3>Perguntas e Respostas - Mercado Livre (<?php echo count($fluxProductQuestion->list_item); ?>)</h3>
      <?php if (isset($_GET['sucesso'])){ echo '<div class="sucesso" style="color:green;">Mensagem respondida com sucesso</div>';}
      if (isset($_GET['problema'])){ echo '<div class="problema" style="color:red;">Mensagem não pôde ser respondida</div>';}?>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <input type="text" name="id" class="form-control" hidden="true" readyonly="true" value="<?php echo $pergunta->id; ?>"/>
            <h2>Produto</h2>
            <label type="text"><a href=<?php echo "$response->permalink";?>><?php echo "$response->permalink";?></a></label>
            <label type="text" name="produto" class="form-control" readyonly="true" value=""><?php echo $response->title; ?></label><br>
            <h2>Data</h2>
            <label type="text" name="data" class="form-control" readyonly="true" value=""><?php echo substr($pergunta->date_created, 0, 10); ?></label>
<label type="text" name="data" class="form-control" readyonly="true" value=""><?php echo "Hora: ".substr($pergunta->date_created, 11); ?></label><br>
            <h2>Pergunta</h2>
            <label type="text" name="pergunta" class="form-control" readyonly="true" value=""><?php echo $pergunta->text; ?></label>
          </div>
          <div class="radio" class="col-md-4">
            <h2>Resposta</h2>
            <textarea name="txtarea" value="resposta" cols="75" rows="5"></textarea><br>

          </div>
          <?php if(isset($_GET['erro'])) echo '<div class="erro" style="color:red;">*Favor digitar a resposta </div><br>'; ?>
            <div class="form-group">
              <input type="submit" name="btnSubmit" class="btnContact" value="Responder" />
            </div>
          </div>

        </div>
      </form>
    </div>
  <?php } else{
     echo '<div class="container contact-form">
    <div class="contact-image">
      <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact"/>
    </div>
    <form method="get" action="perguntas_respostas_be.php">
      <h3>Perguntas e Respostas - Mercado Livre</h3>';
      if (isset($_GET['sucesso'])){ echo '<div class="sucesso" style="color:green;">Mensagem respondida com sucesso</div>';}
      if (isset($_GET['problema'])){ echo '<div class="problema" style="color:red;">Mensagem não pôde ser respondida</div>';}
      echo '<div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <h2>Perguntas</h2>
            <label type="text" name="pergunta" class="form-control" readyonly="true" value="">Nenhuma nova pergunta</label>
          </div>
          </div>

        </div>
      </form>
    </div>';}?>
</body>
