<?php
ini_set("error_reporting",E_ALL);
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once 'include/all_include.php';

$sec_ini = time();

$name = $_POST["titulo"];
$categoryid = $_POST["categoryid"];
$price = $_POST["price"];
$stock = $_POST["stock"];
$description = $_POST["description"];
$images = $_POST["images"];
$sku = $_POST["sku"];
$id = $_POST["id"];



if(is_null($name) || is_null($categoryid) || is_null($price) ||
is_null($stock) || is_null($description) || is_null($images) || is_null($sku) || is_null($id)) {
    header('Location: createProduct.php?erro');
}

echo '<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<pre style="padding:5px;">';

$wcmeliProduct = new meliProduct;

$params = array('access_token' => $wcmeliProduct->accessToken);

$body = [
    "title" => $name,
    "category_id" => $categoryid,
    "price" => $price,
    "currency_id" => "BRL",
    "available_quantity" => $stock,
    "buying_mode" => "buy_it_now",
    "listing_type_id" => "gold_special",
    "condition" => "new",
    "description" => $description,
    "warranty" => "Garantia do vendedor => 90 dias",
    "pictures" => [
        [
            "source" => $images
        ]
    ],
    "attributes" => [
      [
        "id" => "BRAND",
        "name" => "Marca",
        "value_name" => BRAND
      ],
      [
        "id" => "MODEL",
        "name" => "Modelo",
        "value_name" => $sku
      ],
      [
        "id" => "IDWC",
        "name" => "IdWc",
        "value_name" => $id,
        "attribute_group_id" => "OTHERS",
    "attribute_group_name" => "Outros"
      ]
    ]
];

$response = $wcmeliProduct->post("/items",$body, $params);
if($response['httpCode'] >= 200 && $response['httpCode'] < 299) {
  $listSku = json_decode(file_get_contents("include/files/wcmeli_createdproducts_LIST.json"));
  $listSku[] = $sku;
  file_put_contents("include/files/wcmeli_createdproducts_LIST.json",json_encode($listSku));
  $lastSku = ['id'=>$id,'sku'=>$sku];
  file_put_contents("include/files/wcmeli_createproduct",json_encode($lastSku));
  header('Location: createProduct.php?sucesso');
} else {
  foreach ($response['body']->cause as $key => $value) {
    echo $value->message."<br>";
  }
  var_dump($response);
  echo '<a href="createProduct.php?problema"><button>Voltar</button></a>';
}
?>
