<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

class wcmeliProduct extends meliProduct
{
  public function __construct()
  {
    parent::__construct();
    // $this->wooCommerceProduct = new wooCommerceProduct;
  }

  public function wcmeliUpdateProduct($productId,$productEntity)
  {
    if(!is_array($productEntity) && !isset($productEntity->title)) {
      $productSku = $this->meliGetProductSku($productId);
      $wcProductId = $this->wooCommerceProduct->wooCommerceGetProductId(($productSku));
      $productInfo = $this->wooCommerceProduct->wooCommerceGetProduct($wcProductId)->product;


      if(!$productInfo) return "Produto MLB $productId (SKU: $productSku) não foi encontrado no WooCommerce";

      $productName = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/"),explode(" ","a A"),$productInfo->title);
      if(strlen($productName) > 60) $productName = str_replace('ROSQUEAVEL','',strtoupper($productName));
      $title = PREFFIX_PROD.$productName.SUFFIX_PROD;
      if (strlen($title) > 60) $title = PREFFIX_PROD.$productName;

      $settingPrice = ($productInfo->price * SETTINGS_PRICE_MULTIPLICATION) + SETTINGS_PRICE_ADDITION;
      $price = round(($productInfo->price / (1 - COMMISSION)) + $settingPrice ,2);

      $stock = floor($productInfo->stock_quantity + ($productInfo->stock_quantity*SETTINGS_STOCK));

      if($stock < 0) $stock = 0;

      if(TITLE) $productAttributes['title'] = $title;
      else echo '<h1>TITULO DESATIVADO</h1>';
      if(PRICE) $productAttributes['price'] = $price;
      if(STOCK) $productAttributes['available_quantity'] = $stock;
      else echo '<h1>ESTOQUE DESATIVADO</h1>';
      $productAttributes['brand'] = BRAND;
      $productAttributes['sku'] = $productSku;

      if(!empty($productInfo->attributes)) {
        foreach ($productInfo->attributes as $key => $value) {
          if($value->name == 'bundle_title') $productBundleTitle = $value->options[0];
          if($value->name == 'bundle') $productBundle = $value->options[0];
          if($value->name == 'bundle_qty') $productBundleQty = $value->options;
        }
        if($productBundle == 'true') {
          $productBundleTitle = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/"),explode(" ","a A"),$productBundleTitle);
          if(strlen($productBundleTitle) > 60) $productBundleTitle = str_replace('ROSQUEAVEL ','',strtoupper($productBundleTitle));
          $title = PREFFIX_PROD.$productBundleTitle.SUFFIX_PROD;
          if (strlen($title) > 60) $title = PREFFIX_PROD.$productBundleTitle;

          $groupedPrice = 0;

          foreach ($productBundleQty as $key => $value) {
            $productQtyCode = explode('x',$value);
            $groupedProduct = $this->wooCommerceProduct->wooCommerceGetProduct($productQtyCode[1])->product;

            $groupedPrice += $groupedProduct->price*(int)$productQtyCode[0];

            $groupedQty = $groupedProduct->stock_quantity;

            if(isset($minGroupedQty) && $minGroupedQty < $groupedQty) $minGroupedQty = $minGroupedQty;
            // else if(!isset($minGroupedQty)) $minGroupedQty = 0;
            else $minGroupedQty = $groupedQty;
          }

          $settingPrice = ($groupedPrice * SETTINGS_PRICE_MULTIPLICATION) + SETTINGS_PRICE_ADDITION;
          $price = round(($groupedPrice / (1 - COMMISSION)) + $settingPrice ,2);

          $minGroupedQty = $minGroupedQty + ($minGroupedQty*SETTINGS_STOCK);
          $stock = floor($minGroupedQty/$productQtyCode[0]);

          if($stock < 0) $stock = 0;

          if(TITLE) $productAttributes['title'] = $title;
          else echo '<h1>TITULO DESATIVADO</h1>';
          if(PRICE) $productAttributes['price'] = $price;
          if(STOCK) $productAttributes['available_quantity'] = $stock;
          else echo '<h1>ESTOQUE DESATIVADO</h1>';
          $productAttributes['brand'] = BRAND;
          $productAttributes['sku'] = $productSku;
        }
      }

      echo "<b>$productId    $productSku</b><br>";
      echo "<b>".$productAttributes['title']."(".strlen($productAttributes['title']).")</b><br>";
      echo 'PREÇO: '.$productAttributes['price'].'<br>';
      echo 'ESTOQUE: '.$productAttributes['available_quantity'].'<br>';

      $updateProduct = $this->meliUpdateProduct($productId,$productAttributes);
      if($updateProduct['httpCode'] >= 200 && $updateProduct['httpCode'] <= 299) echo "<b>Attributes: OK</b><br>";
      else {
        $error = new error_handling("WCML: Script Mercado Livre - Erro Atributos Produto", "Erro ao atualizar titulo/preço/estoque/marca/sku do produto SKU:$productSku - MLB:$productId", $updateProduct['body']->message."HttpCode ".$updateProduct['httpCode'], "Erro produto");
        $error->send_error_email();
        $error->execute();

        return "Erro ao atualizar produto mlb: $productId || sku: $productSku";
      }


      $productDescription = $productInfo->description;
      $updateProductDescription = $this->meliUpdateProductDescription($productId,$productDescription);
      if($updateProductDescription['httpCode'] >= 200 && $updateProductDescription['httpCode'] <= 299) echo "<b>Description: OK</b><br>";
      else {
        $error = new error_handling("WCML: Script Mercado Livre - Erro Descrição Produto", "Erro ao atualizar descrição do produto SKU:$productSku - MLB:$productId", $updateProductDescription['body']->message."HttpCode ".$updateProductDescription['httpCode'], "Erro produto");
        $error->send_error_email();
        $error->execute();
        return "Erro ao atualizar produto mlb: $productId || sku: $productSku";
      }
    } else {
      $fluxProduct = new flux('wcmeli_product');
      $fluxProduct->timeFile = true;
      $fluxProduct->setFiles();

      $fluxTimer = $fluxProduct->getTimer();
      $updateProduct = $this->meliUpdateProduct($productId,$productEntity);

      if(isset($productEntity['description']))
      $updateProductDescription = $this->meliUpdateProductDescription($productId,$productEntity['description']);

      if($updateProduct['httpCode'] > 399) {
        $erroProduct = 'Produto';
        // Log any exceptions to a WC logger
        $log = new WC_Logger();
        $log_entry = 'Exception Trace: ';
        $log_entry .= print_r( $updateProduct['body']->message, true );
        $log->add( 'new-woocommerce-log-name', $log_entry );

        if($timerHelper['product'] + SEND_TIME <= time()) {
          $error = new error_handling("WcSa2Meli: Informações do Produto", "Algo impossibilitou a atualização do produto SKU:$productSku - MLB:$productId", $updateProductDescription['body']->message."HttpCode ".$updateProductDescription['httpCode'], "Erro produto");
          $error->send_mail = true;
          $error->send_error_email();
          $error->execute();
          $fluxProduct->addTimer('product',time());
        }
      } elseif($updateProductDescription['httpCode'] > 399) {
        $erroDescription = 'Descrição';
        // Log any exceptions to a WC logger
        $log = new WC_Logger();
        $log_entry .= 'Exception Trace: ';
        $log_entry = print_r( $updateProductDescription['body']->message, true );
        $log->add( 'new-woocommerce-log-name', $log_entry );

        if($timerHelper['product'] + SEND_TIME <= time()) {
          $error = new error_handling("WcSa2Meli: Descrição do Produto", "Algo impossibilitou a atualização do produto SKU:$productSku - MLB:$productId", $updateProductDescription['body']->message."HttpCode ".$updateProductDescription['httpCode'], "Erro produto");
          $error->send_mail = true;
          $error->send_error_email();
          $error->execute();
          $fluxProduct->addTimer('product',time());
        }
      } else return true;
    }
  }

  public function updatewcsa2meli($string, $secret_key = 'This is my secret key', $secret_iv = 'This is my secret iv') {
      $output = false;$encrypt_method = "AES-256-CBC";$key = hash('sha256', $secret_key);$iv = substr(hash('sha256', $secret_iv), 0, 16);$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);$output = base64_encode($output);return $output;
  }
}
?>
