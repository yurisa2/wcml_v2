<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

define("PATHFILES",$pathfiles);
if(isset($user_id)) define("USER_ID",$user_id);

define("SETTINGS_PRICE_MULTIPLICATION",$settingsPriceMultiplication);
define("SETTINGS_PRICE_ADDITION",$settingPriceAddition);
define("SETTINGS_STOCK",$settingsStock);
define("SUFFIX_PROD",$suffixProd);
define("PREFFIX_PROD",$preffixProd);
define("BRAND",$brand);
define("COMMISSION",$commission);

//KEY TO SINCRONIZATION OF PRODUCTS AND ORDER
define("SYNCPROD",$syncProd);
define("TITLE",$title);
define("PRICE",$price);
define("DESCRIPTION",$description);
define("STOCK",$stock);
define("IMAGES",$images);
define("ORDER",$order);

define("UPDATE_IMAGES",$updateImages);
define("SEND_TIME",$sendEmail);

//MANDAR EMAIL
define("CONFIGMAIL",$configmail);     // true para habilitar o envio de email
define("EMAIL_TO",$emailTo);
define("EMAIL_NEWSALE",$emailNewSale);
define("EMAIL_NEWQUESTION",$emailNewQuestion);
define("EMAIL_QUESTIONAFTERSALE",$emailQuestionAfterSale);

define("SENDEMAIL_NEWQUESTION",$SendEmailNewQuestion);
// Ainda há problemas não encontrados para o uso do Sendmail
define("SMTP",$SMTP);     //if SMTP equals false, sendmail will be used

//if SMTP equals true, the variaveis below will need be set up
define("HOST",$Host);     // Specify main and backup SMTP servers
define("SMTPAUTH",$SMTPAuth);     // Enable SMTP authentication
define("USERNAME",$Username);     // SMTP username
define("PASSWORD",$Password);     // SMTP password
define("SMTPSECURE",$SMTPSecure);     // Enable TLS encryption, `ssl` also accepted
