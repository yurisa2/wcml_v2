<?php
class wcmeliOrder extends meliOrder
{
  public $updateImage = false;
  public $normalizeCustomerData;
  public $normalizeOrderData;

  public function __construct()
  {
    parent::__construct();
    $this->meliProduct = new meliProduct;
    // $this->wooCommerceOrder = new wooCommerceOrder;
    // $this->wooCommerceProduct = new wooCommerceProduct;
    // $this->wooCommerceCustomer = new wooCommerceCustomer;
  }

  public function wcmeliCreateOrder($orderId,$orderEntity)
  {
    if(!is_array($orderEntity) && !isset($orderEntity->id)) {
    if(is_array($orderId)) {
      foreach ($orderId as $key => $value) $orderData[] = $this->meliGetOrder($value)['body'];
    } else {
      $orderData[] = $this->meliGetOrder($orderId)['body'];
    }

    $this->normalizeCustomerData($orderData);

    $customerList = $this->wooCommerceCustomer->WooCommerceGetCustomerList(['email'=>$this->normalizeCustomerData->email]);

    if(empty($customerList)) $orderData[0]->wcCustomerId = $this->wooCommerceCustomer->wooCommerceCreateCustomer($this->normalizeCustomerData)->id;
    else $orderData[0]->wcCustomerId = $customerList[0]->id;

    $this->normalizeOrderData($orderData);

    try {
      $createOrder = $this->wooCommerceOrder->wooCommerceCreateOrder($this->normalizeOrderData);
    } catch(Exception $exception) {
      $error = new error_handling("WCML: Erro ao criar pedido no WooCommerce", (string)$exception->getMessage(), "Pedido: $orderData->order_id", "Erro pedido");
      $error->send_error_email();
      $error->execute();
      echo '<br>'.$exception->getMessage();
      return false;
    }

    $labelUrl = $this->meliGetOrderLabel($this->normalizeCustomerData->shippingId, json_encode($orderId));

    copy($labelUrl,str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.json_encode($orderId).".pdf");
    // pega o nome do comprador

    $nome = $this->normalizeCustomerData->firstName.' '.$this->normalizeCustomerData->lastName ;
    $error_handling = new log("Novo Pedido Mercado Livre", "Numero do Pedido: ".json_encode($orderId), "Comprador: $nome", "nova compra");
    $error_handling->log_email = true;
    $error_handling->mensagem_email = "Nova compra Mercado Livre";
    $error_handling->log_email = true;
    $error_handling->email_novacompra = true;
    $error_handling->dir_file = "log/lo g.json";
    $error_handling->log_files = true;
    $error_handling->send_log_email();
    $error_handling->execute();

    return $createOrder;
  } else {
    $fluxOrder = new flux('wcmeli_order');
    $fluxOrder->timeFile = true;
    $fluxOrder->setFiles();

    $fluxTimer = $fluxOrder->getTimer();
    try {
      $createOrder = $this->wooCommerceOrder->wooCommerceCreateOrder($this->normalizeOrderData);
    } catch(Exception $exception) {
      $log = new WC_Logger();
      $log_entry = 'Exception Trace: ';
      $log_entry .= print_r( (string)$exception->getMessage(), true );
      $log->add( 'new-woocommerce-log-name', $log_entry );

      if($timerHelper['product'] + SEND_TIME <= time()) {
      $error = new error_handling("WCML: Erro ao criar pedido no WooCommerce", (string)$exception->getMessage(), "Pedido: $orderData->order_id", "Erro pedido");
      $error->send_mail = true;
      $error->send_error_email();
      $error->execute();
      $fluxOrder->addTimer('order',time());
      }
      echo '<br>'.$exception->getMessage();
      return false;
    }
    return true;
  }
  }

  public function normalizeCustomerData($orderData)
  {
    foreach ($orderData as $key => $value) {
      $nameLastname = ucwords(strtolower($value->buyer->first_name.' '.$value->buyer->last_name));
      $name = explode(' ', $nameLastname);
      $lastname = array_splice($name, -1);
      $name = implode(' ',$name);
      $lastname = implode(' ',$lastname);

      $username = explode(' ', strtolower($value->buyer->first_name.' '.$value->buyer->last_name));
      array_splice($username, 1);
      $username = strtolower(implode('',$username).'.'.$lastname);
      $email = $value->buyer->email;
      $phone = trim($value->buyer->phone->area_code.$value->buyer->phone->number);
      $cpf =  $value->buyer->billing_info->doc_number;
      $this->normalizeCustomerData->email = $email;
      $this->normalizeCustomerData->firstName = "MLB ".$name;
      $this->normalizeCustomerData->lastName = $lastname;
      $this->normalizeCustomerData->username = $username;

      if(!isset($value->shipping->receiver_address)){
        if(isset($value->shipping->id)) {
        $shipmentId = $value->shipping->id;
        $params = array('access_token' => $this->access_token);
        $shippingData = $this->meli->get("/shipments/$shipmentId", $params);

        $receiver = $shippingData['body']->receiver_address->receiver_name;
        $street = $shippingData['body']->receiver_address->street_name;
        $number =$shippingData['body']->receiver_address->street_number;
        $neighborhood = $shippingData['body']->receiver_address->neighborhood->name;
        $complement = $shippingData['body']->receiver_address->comment;
        $postcode = $shippingData['body']->receiver_address->zip_code;
        $city = $shippingData['body']->receiver_address->city->name;
        $state = $shippingData['body']->receiver_address->state->id;
        $state = substr($state,-2);
        $country = $shippingData['body']->receiver_address->country->id;
        $shippingId =  $shippingData['body']->receiver_address->id;
      } else {
        $receiver = $value->shipping->status;
        $street = $value->shipping->status;
        $number = $value->shipping->status;
        $neighborhood = $value->shipping->status;
        $complement = $value->shipping->status;
        $postcode = $value->shipping->status;
        $city = $value->shipping->status;
        $state = $value->shipping->status;
        $state = $value->shipping->status;
        $country = $value->shipping->status;
        $shippingId = $value->shipping->status;
      }
      } else {
        $receiver = $value->shipping->receiver_address->receiver_name;
        $street = $value->shipping->receiver_address->street_name;
        $number =$value->shipping->receiver_address->street_number;
        $neighborhood = $value->shipping->receiver_address->neighborhood->name;
        $complement = $value->shipping->receiver_address->comment;
        $postcode = $value->shipping->receiver_address->zip_code;
        $city = $value->shipping->receiver_address->city->name;
        $state = $value->shipping->receiver_address->state->id;
        $state = substr($state,-2);
        $country = $value->shipping->receiver_address->country->id;
        $shippingId = $value->shipping->id;
      }
    }
    if(!empty($neighborhood)) $neighborhoodAddress = '-'.$neighborhood;

    $this->normalizeCustomerData->billingFirstName = "MLB ".$receiver;
    $this->normalizeCustomerData->billingLastname = '';
    $this->normalizeCustomerData->billingAddress = $street.', '.$number.$neighborhoodAddress;
    $this->normalizeCustomerData->billingAddress2 = $complement;
    $this->normalizeCustomerData->billingStreet = $street;
    $this->normalizeCustomerData->billingNumber = $number;
    $this->normalizeCustomerData->billingNeighborhood = $neighborhood;
    $this->normalizeCustomerData->billingCity = $city;
    $this->normalizeCustomerData->billingState = $state;
    $this->normalizeCustomerData->billingCep = $postcode;
    $this->normalizeCustomerData->billingCountry = $country;
    $this->normalizeCustomerData->billingEmail = $email;
    $this->normalizeCustomerData->billingPhone = $phone;
    $this->normalizeCustomerData->shippingFirstName = "MLB ".$receiver;
    $this->normalizeCustomerData->shippingLastname = '';
    $this->normalizeCustomerData->shippingAddress = $street.', '.$number.$neighborhoodAddress;
    $this->normalizeCustomerData->shippingAddress2 = $complement;
    $this->normalizeCustomerData->shippingStreet = $street;
    $this->normalizeCustomerData->shippingNumber = $number;
    $this->normalizeCustomerData->shippingNeighborhood = $neighborhood;
    $this->normalizeCustomerData->shippingCity = $city;
    $this->normalizeCustomerData->shippingState = $state;
    $this->normalizeCustomerData->shippingCep = $postcode;
    $this->normalizeCustomerData->shippingCountry = $country;
    $this->normalizeCustomerData->cpf = $cpf;
    $this->normalizeCustomerData->shippingId = $shippingId;

    // var_dump($this->normalizeCustomerData);         //DEBUG
    // exit;                                           //DEBUG
    return $this->normalizeCustomerData;
  }

  public function normalizeOrderData($orderData)
  {
    $productSpecialPrice = '';
    $productOriginalPrice = '';
    $payment = 0;
    $parcels = 0;
    $subtotal = 0;
    $shippingCost = 0;
    $totalPaid = 0;
    $commissionTotal = 0;
    $productQty = '';
    $productId = '';


    foreach ($orderData as $key => $orderValue) {
      $meliOrderId[] = $orderValue->id;

      foreach ($orderValue->payments as $key => $paymentValue) {
        $paymentType = $paymentValue->payment_type;
        $parcels += $paymentValue->installments;
        $shippingCost += $paymentValue->shipping_cost;
        $totalPaid += $paymentValue->total_paid_amount;
      }

      $recentOrders = $this->meliGetRecentOrders()['body']->results;
      $orderId = $orderValue->id;
      foreach ($recentOrders as $key => $recentOrdersValue) {
        if($recentOrdersValue->id == $orderValue->id) {
          foreach ($recentOrdersValue->payments as $key => $recentPaymentValue) {
            $commission = $recentPaymentValue->marketplace_fee;
            $commissionTotal += $recentPaymentValue->marketplace_fee;
          }
          foreach ($recentOrdersValue->order_items as $key => $itemValue) {
            $subtotal += $itemValue->quantity*$itemValue->unit_price;
            $productSku = $this->meliProduct->meliGetProductSku($itemValue->item->id);
            $wooCommerceProductInfo = $this->wooCommerceProduct->wooCommerceGetProduct($this->wooCommerceProduct->wooCommerceGetProductId($productSku));
            foreach ($wooCommerceProductInfo->product->attributes as $key => $value) {
              if($value->name == 'bundle_qty') {
                $productQtyId = explode('x',$value->options[0]);
                $productQty = $productQtyId[0];
                $productId = $productQtyId[1];
              }
            }
            if(empty($productQty) || empty($productId)) {
              $productId = $this->wooCommerceProduct->wooCommerceGetProductId($productSku);
              $productQty = $itemValue->quantity;
            }
            $this->normalizeOrderData->productItems[] = [
              'product_id' => $productId,
              'quantity' => $productQty,
              // 'total' => $itemValue->quantity * $itemValue->unit_price
              'total' => $itemValue->quantity * $itemValue->unit_price - $commission
            ];
          }

        }
      }
    }

    // $totalPaid = $totalPaid - $commissionTotal - $shippingCost;
    // $labelUrl = $this->meliGetOrderLabel($this->normalizeCustomerData->shippingId, json_encode($orderId));

    $this->normalizeOrderData->meta_data[] = ['key' => 'Tipo de pagamento', 'value' => $paymentType];

    $this->normalizeOrderData->meta_data[] = ['key' => 'totalToPay', 'value' => $totalPaid];
    $this->normalizeOrderData->meta_data[] = ['key' => 'Parcelas', 'value' => $parcels];

    // $this->normalizeOrderData->meta_data[] = ['key' => 'birthday', 'value' => $orderData->customer->date_of_birth];

    // $this->normalizeOrderData->meta_data[] = ['key' => 'shippingReference', 'value' => $orderData->shipping_address->reference];
    // $this->normalizeOrderData->meta_data[] = ['key' => 'billingReference', 'value' => $orderData->billing_address->reference];

    $this->normalizeOrderData->shippingName = $this->normalizeCustomerData->firstName;
    $this->normalizeOrderData->shippingLastname = $this->normalizeCustomerData->lastName;
    $this->normalizeOrderData->shippingAddress = $this->normalizeCustomerData->shippingAddress;
    $this->normalizeOrderData->shippingAddress2 = $this->normalizeCustomerData->shippingAddress2;
    $this->normalizeOrderData->shippingCity = $this->normalizeCustomerData->shippingCity;
    $this->normalizeOrderData->shippingState = $this->normalizeCustomerData->shippingState;
    $this->normalizeOrderData->shippingCep = $this->normalizeCustomerData->shippingCep;
    $this->normalizeOrderData->shippingCountry = $this->normalizeCustomerData->shippingCountry;

    $this->normalizeOrderData->billingName = $this->normalizeCustomerData->firstName;
    $this->normalizeOrderData->billingLastname = $this->normalizeCustomerData->lastName;
    $this->normalizeOrderData->billingAddress = $this->normalizeCustomerData->billingAddress;
    $this->normalizeOrderData->billingAddress2 = $this->normalizeCustomerData->billingAddress2;
    $this->normalizeOrderData->billingCity = $this->normalizeCustomerData->billingCity;
    $this->normalizeOrderData->billingState = $this->normalizeCustomerData->billingState;
    $this->normalizeOrderData->billingCep = $this->normalizeCustomerData->billingCep;
    $this->normalizeOrderData->billingCountry = $this->normalizeCustomerData->billingCountry;
    $this->normalizeOrderData->billingEmail = $this->normalizeCustomerData->billingEmail;

    $this->normalizeOrderData->billingPhone = $this->normalizeCustomerData->billingPhone;

    $this->normalizeOrderData->shippingTotal = 0;

    $this->normalizeOrderData->customerId = $orderData[0]->wcCustomerId;

    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_cpf', 'value' => $this->normalizeCustomerData->cpf];
    $this->normalizeOrderData->meta_data[] = ['key' => '_shipping_number', 'value' => $this->normalizeCustomerData->shippingNumber];
    $this->normalizeOrderData->meta_data[] = ['key' => '_shipping_neighborhood', 'value' => $this->normalizeCustomerData->shippingNeighborhood];
    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_neighborhood', 'value' => $this->normalizeCustomerData->billingNeighborhood];
    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_number', 'value' => $this->normalizeCustomerData->billingNumber];


    $this->normalizeOrderData->meta_data[] = ['key' => 'orderId', 'value' => json_encode($meliOrderId)];
    $this->normalizeOrderData->meta_data[] = ['key' => 'subtotal', 'value' => $subtotal];
    // $this->normalizeOrderData->meta_data[] = ['key' => 'discount', 'value' => 0];
    $this->normalizeOrderData->meta_data[] = ['key' => 'commission', 'value' => $commissionTotal];
    $this->normalizeOrderData->meta_data[] = ['key' => 'shipping', 'value' => $shippingCost];

    $this->normalizeOrderData->meta_data[] = ['key' => 'etiqueta', 'value' => file_get_contents($labelUrl)];

    // var_dump($this->normalizeOrderData);         //DEBUG
    // exit('OrderData');                                           //DEBUG
    return $this->normalizeOrderData;
  }

  public function normalizeOrderDataWc($orderData)
  {
    $productSpecialPrice = '';
    $productOriginalPrice = '';
    $payment = 0;
    $parcels = 0;
    $subtotal = 0;
    $shippingCost = 0;
    $totalPaid = 0;
    $commissionTotal = 0;
    $productQty = '';
    $productId = '';


      foreach ($orderData as $key => $orderValue) {
        $meliOrderId[] = $orderValue->id;

        foreach ($orderValue->payments as $key => $paymentValue) {
          $paymentType = $paymentValue->payment_type;
          $parcels += $paymentValue->installments;
          $shippingCost += $paymentValue->shipping_cost;
          $totalPaid += $paymentValue->total_paid_amount;
        }

        $recentOrders = $this->meliGetRecentOrders()['body']->results;
        $orderId = $orderValue->id;
        foreach ($recentOrders as $key => $recentOrdersValue) {
          if($recentOrdersValue->id == $orderValue->id) {
            foreach ($recentOrdersValue->payments as $key => $recentPaymentValue) {
              $commission = $recentPaymentValue->marketplace_fee;
              $commissionTotal += $recentPaymentValue->marketplace_fee;
            }
            foreach ($recentOrdersValue->order_items as $key => $itemValue) {
              $subtotal += $itemValue->quantity*$itemValue->unit_price;
              $productSku = $this->meliProduct->meliGetProductSku($itemValue->item->id);
              $wooCommerceProductInfo = wc_get_product_id_by_sku($productSku);
              if(empty($productQty) || empty($productId)) {
                $productId = wc_get_product_id_by_sku($productSku);
                $productQty = $itemValue->quantity;
              }
              $this->normalizeOrderData->productItems[] = [
                'product_id' => $productId,
                'quantity' => $productQty,
                // 'total' => $itemValue->quantity * $itemValue->unit_price
                'total' => $itemValue->quantity * $itemValue->unit_price - $commission
              ];
            }

          }
        }
      }


    // $totalPaid = $totalPaid - $commissionTotal - $shippingCost;
    // $labelUrl = $this->meliGetOrderLabel($this->normalizeCustomerData->shippingId, json_encode($orderId));

    $this->normalizeOrderData->meta_data[] = ['key' => 'Tipo de pagamento', 'value' => $paymentType];

    $this->normalizeOrderData->meta_data[] = ['key' => 'totalToPay', 'value' => $totalPaid];
    $this->normalizeOrderData->meta_data[] = ['key' => 'Parcelas', 'value' => $parcels];

    // $this->normalizeOrderData->meta_data[] = ['key' => 'birthday', 'value' => $orderData->customer->date_of_birth];

    // $this->normalizeOrderData->meta_data[] = ['key' => 'shippingReference', 'value' => $orderData->shipping_address->reference];
    // $this->normalizeOrderData->meta_data[] = ['key' => 'billingReference', 'value' => $orderData->billing_address->reference];

    $this->normalizeOrderData->shippingName = $this->normalizeCustomerData->firstName;
    $this->normalizeOrderData->shippingLastname = $this->normalizeCustomerData->lastName;
    $this->normalizeOrderData->shippingAddress = $this->normalizeCustomerData->shippingAddress;
    $this->normalizeOrderData->shippingAddress2 = $this->normalizeCustomerData->shippingAddress2;
    $this->normalizeOrderData->shippingCity = $this->normalizeCustomerData->shippingCity;
    $this->normalizeOrderData->shippingState = $this->normalizeCustomerData->shippingState;
    $this->normalizeOrderData->shippingCep = $this->normalizeCustomerData->shippingCep;
    $this->normalizeOrderData->shippingCountry = $this->normalizeCustomerData->shippingCountry;

    $this->normalizeOrderData->billingName = $this->normalizeCustomerData->firstName;
    $this->normalizeOrderData->billingLastname = $this->normalizeCustomerData->lastName;
    $this->normalizeOrderData->billingAddress = $this->normalizeCustomerData->billingAddress;
    $this->normalizeOrderData->billingAddress2 = $this->normalizeCustomerData->billingAddress2;
    $this->normalizeOrderData->billingCity = $this->normalizeCustomerData->billingCity;
    $this->normalizeOrderData->billingState = $this->normalizeCustomerData->billingState;
    $this->normalizeOrderData->billingCep = $this->normalizeCustomerData->billingCep;
    $this->normalizeOrderData->billingCountry = $this->normalizeCustomerData->billingCountry;
    $this->normalizeOrderData->billingEmail = $this->normalizeCustomerData->billingEmail;

    $this->normalizeOrderData->billingPhone = $this->normalizeCustomerData->billingPhone;

    $this->normalizeOrderData->shippingTotal = 0;

    $this->normalizeOrderData->customerId = $orderData[0]->wcCustomerId;

    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_cpf', 'value' => $this->normalizeCustomerData->cpf];
    $this->normalizeOrderData->meta_data[] = ['key' => '_shipping_number', 'value' => $this->normalizeCustomerData->shippingNumber];
    $this->normalizeOrderData->meta_data[] = ['key' => '_shipping_neighborhood', 'value' => $this->normalizeCustomerData->shippingNeighborhood];
    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_neighborhood', 'value' => $this->normalizeCustomerData->billingNeighborhood];
    $this->normalizeOrderData->meta_data[] = ['key' => '_billing_number', 'value' => $this->normalizeCustomerData->billingNumber];


    $this->normalizeOrderData->meta_data[] = ['key' => 'orderId', 'value' => json_encode($meliOrderId)];
    $this->normalizeOrderData->meta_data[] = ['key' => 'subtotal', 'value' => $subtotal];
    // $this->normalizeOrderData->meta_data[] = ['key' => 'discount', 'value' => 0];
    $this->normalizeOrderData->meta_data[] = ['key' => 'commission', 'value' => $commissionTotal];
    $this->normalizeOrderData->meta_data[] = ['key' => 'shipping', 'value' => $shippingCost];

    // $this->normalizeOrderData->meta_data[] = ['key' => 'etiqueta', 'value' => file_get_contents($labelUrl)];

    // var_dump($this->normalizeOrderData);         //DEBUG
    // exit('OrderData');                                           //DEBUG
    return $this->normalizeOrderData;
  }

  public function wcmeliOrderExists($mktplaceOrderId,$limit)
  {
    try {
      $salesOrder = $this->wooCommerceOrder->wooCommerceGetOrders(['order'=>'desc','per_page'=>$limit]);
      // var_dump($salesOrder);
      if(count($salesOrder->orders) <= $limit) {
        foreach ($salesOrder->orders as $key => $value) {
          $orderData = $this->wooCommerceOrder->wooCommerceGetOrder($value->id);
          foreach ($orderData->meta_data as $key => $value) {
            if($value->key == "orderId" && json_encode($value->value) == $mktplaceOrderId) return true;
          }
        }
        return false;
      }

      foreach ($salesOrder->orders as $key => $value) {
        if($key < count($salesOrder->orders)-$limit) $last_orders[] = $value;
      }
      foreach ($last_orders as $key => $value) {
        $orderData = $this->wooCommerceOrder->wooCommerceGetOrder($value->id);
        foreach ($orderData->meta_data as $key => $value) {
          if(json_encode($value->value) == $mktplaceOrderId) return true;
        }
      }
      return false;
    } catch(Exception $e) {
      $error = new error_handling("WCB2W: Erro ao buscar pedidos","Tentativa falha de buscar a lista de pedidos", "Erro<br>".$e->getMessage(), "Erro Pedido");
      $error->send_error_email();
      $error->execute();
      exit("Skyhub com problemas. Não foi possível retornar os pedidos");
    }
  }
}
?>
