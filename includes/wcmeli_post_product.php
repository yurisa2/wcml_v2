<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}

include_once $wcmeli_plugin_dir.'includes/wcmeli_get.php';
$productAttributes['mlb'] = $productMlb;
$productAttributes['token'] = $wcmeliProduct->accessToken;

$productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

$postdata = http_build_query(
  [
    'query' =>  ($productData)
  ]
);


$opts = array('http' =>
array(
  'method'  => 'POST',
  'header'  => "Content-Type: application/x-www-form-urlencoded",
  'content' => $postdata
)
);
$context  = stream_context_create($opts);

$url = 'https://sa2.com.br/wcsa2meli/wcsa2meli.php';
$result = file_get_contents($url, false, $context);

$result = (array)json_decode($result,true);

// exit("wcmeli_post_product");
?>
