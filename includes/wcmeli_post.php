<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}

if(file_exists($wcmeli_plugin_dir.'include/files/noideas')) {

  $noidea = base64_encode(json_encode(['context'=>['f'=>base64_encode(get_site_url()),'c'=>file_get_contents($wcmeli_plugin_dir.'include/files/noideas')]]));

$postdata = http_build_query(
  [
    'q' =>  ($noidea)
  ]
);


$opts = array('http' =>
array(
  'method'  => 'POST',
  'header'  => "Content-Type: application/x-www-form-urlencoded",
  'content' => $postdata
)
);
$context  = stream_context_create($opts);

$url = 'https://sa2.com.br/wcsa2meli/wcsa2meli.php';
$result = file_get_contents($url, false, $context);

if($result){
  unlink($wcmeli_plugin_dir.'include/files/noideas');
}
}

?>
