<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}
if(ORDER) {
echo "<h1>PEDIDO</h1>";
// exit;

$wcmeliOrder = new wcmeliOrder;
$fluxOrder = new flux('wcmeli_order');
// $fluxOrder->list_item = $listProductId;
$fluxOrder->timeFile = true;
$fluxOrder->nfeFile = false;
$fluxOrder->storeOrderList = true;
$fluxOrder->pathListItem = true;
if(!$fluxOrder->setFiles()) {
  $listProductId = $wcmeliOrder->meliGetOrderIds();
  $fluxOrder->list_item = $listProductId;
  $fluxOrder->setFiles();
}
$fluxOrder->getFiles();
$order_id = $fluxOrder->test_next_item();

// $order_id = 2174836150;

if(!$order_id) echo "Não há novos pedidos<br>";
else {
    if(is_array($order_id)) var_dump($order_id);
    else echo "<h3>$order_id</h3>";
    try {
      $arg = array( 'limit' => 10,'order' => 'desc', 'return' => 'objects' ) ;
      $lastOrders = wc_get_orders($arg);
      $found = false;
        foreach ($lastOrders as $key => $value) {
          foreach ($value->get_data()['meta_data'] as $key => $meta_data) {
            if($meta_data->__get( 'key' ) == "orderId") {
              if(is_array($meta_data->__get( 'value' ))) {
                if(json_encode($meta_data->__get( 'value' )) == $order_id) $found = true;
              } else {
                if($meta_data->__get( 'value' ) == $order_id) $found = true;
              }
             }
          }
        }

} catch(Exception $e) {
  $error = new error_handling("WCB2W: Erro ao buscar pedidos","Tentativa falha de buscar a lista de pedidos", "Erro<br>".$e->getMessage(), "Erro Pedido");
  $error->send_error_email();
  $error->execute();
  exit("Skyhub com problemas. Não foi possível retornar os pedidos");
}

    if(array_search($order_id,$fluxOrder->getOrderStoreList()) === false && !$found) {

      include_once $wcmeli_plugin_dir.'includes/wcmeli_get.php';

      $orderAttributes['order']['id'] = $order_id;
      $orderAttributes['token'] = $wcmeliProduct->accessToken;

      $productData = $wcmeliProduct->updatewcsa2meli(json_encode($orderAttributes),base64_decode($return[0]),base64_decode($return[1]));

      $content = file_get_contents('https://sa2.com.br/wcsa2meli/wcsa2meli.php?query='.$productData);
      $orderData = (array)json_decode($content);


      $wcmeliOrder->normalizeCustomerData($orderData['order']);

      $users = get_users([
        'search'  => $wcmeliOrder->normalizeCustomerData->username,
        'orderby'       => 'ID'
      ]);
      $orderLabelAttributes['label']['id'] = $order_id;
      $orderLabelAttributes['label']['shipment_ids'] = $wcmeliOrder->normalizeCustomerData->shippingId;
      $orderLabelAttributes['token'] = $wcmeliProduct->accessToken;

      $productData = $wcmeliProduct->updatewcsa2meli(json_encode($orderLabelAttributes),base64_decode($return[0]),base64_decode($return[1]));

      $content = file_get_contents('https://sa2.com.br/wcsa2meli/wcsa2meli.php?query='.$productData);
// if(strlen($content) > 500) var_dump($content);
//     exit('label');

      if(empty($users)) {



          $user_id = wc_create_new_customer( $wcmeliOrder->normalizeCustomerData->email, $wcmeliOrder->normalizeCustomerData->username, $wcmeliOrder->normalizeCustomerData->username);

    update_user_meta( $user_id, "billing_first_name", $wcmeliOrder->normalizeCustomerData->billingFirstName );
    update_user_meta( $user_id, "billing_last_name", $wcmeliOrder->normalizeCustomerData->billingLastname );
    update_user_meta( $user_id, "billing_address_1", $wcmeliOrder->normalizeCustomerData->billingAddress );
    update_user_meta( $user_id, "billing_address_2", $wcmeliOrder->normalizeCustomerData->billingAddress2 );
    update_user_meta( $user_id, "billing_city", $wcmeliOrder->normalizeCustomerData->billingCity );
    update_user_meta( $user_id, "billing_postcode", $wcmeliOrder->normalizeCustomerData->billingCep );
    update_user_meta( $user_id, "billing_country", $wcmeliOrder->normalizeCustomerData->billingCountry );
    update_user_meta( $user_id, "billing_state", $wcmeliOrder->normalizeCustomerData->billingState );
    update_user_meta( $user_id, "billing_email", $wcmeliOrder->normalizeCustomerData->billingEmail );
    update_user_meta( $user_id, "billing_phone", $wcmeliOrder->normalizeCustomerData->billingPhone );

    update_user_meta( $user_id, "shipping_first_name", $wcmeliOrder->normalizeCustomerData->billingFirstName );
    update_user_meta( $user_id, "shipping_last_name", $wcmeliOrder->normalizeCustomerData->billingLastname );
    update_user_meta( $user_id, "shipping_city", $wcmeliOrder->normalizeCustomerData->billingCity );
    update_user_meta( $user_id, "shipping_postcode", $wcmeliOrder->normalizeCustomerData->billingCep );
    update_user_meta( $user_id, "shipping_country", $wcmeliOrder->normalizeCustomerData->billingCountry );
    update_user_meta( $user_id, "shipping_state", $wcmeliOrder->normalizeCustomerData->billingState );

    update_user_meta( $user_id, "meta_data", [ ['key'=>'CPF', 'value'=>$wcmeliOrder->normalizeCustomerData->cpf] ] );
        $orderData['order'][0]->wcCustomerId = $user_id;

      }
      else $orderData['order'][0]->wcCustomerId = $users[0]->id;

      $wcmeliOrder->normalizeOrderDataWc($orderData['order']);


      try {
        $shipping = array(
              'first_name' => $wcmeliOrder->normalizeOrderData->shippingName,
              'last_name'  => $wcmeliOrder->normalizeOrderData->shippingLastname,
              'company'    => '',
              'address_1'  => $wcmeliOrder->normalizeOrderData->shippingAddress,
              'address_2'  => $wcmeliOrder->normalizeOrderData->shippingAddress2,
              'city'       => $wcmeliOrder->normalizeOrderData->shippingCity,
              'state'      => $wcmeliOrder->normalizeOrderData->shippingState,
              'state'      => $wcmeliOrder->normalizeOrderData->shippingState,
              'postcode'   => $wcmeliOrder->normalizeOrderData->shippingCep,
              'country'    => $wcmeliOrder->normalizeOrderData->shippingCountry
          );
        $billing = array(
          'first_name' => $wcmeliOrder->normalizeOrderData->billingName,
          'last_name'  => $wcmeliOrder->normalizeOrderData->billingLastname,
          'company'    => '',
          'email'      => $wcmeliOrder->normalizeOrderData->billingEmail,
          'phone'      => $wcmeliOrder->normalizeOrderData->billingPhone,
          'address_1'  => $wcmeliOrder->normalizeOrderData->billingAddress,
          'address_2'  => $wcmeliOrder->normalizeOrderData->billingAddress2,
          'city'       => $wcmeliOrder->normalizeOrderData->billingCity,
          'state'      => $wcmeliOrder->normalizeOrderData->billingState,
          'postcode'   => $wcmeliOrder->normalizeOrderData->billingCep,
          'country'    => $wcmeliOrder->normalizeOrderData->billingCountry
          );

          // Now we create the order
          $order = wc_create_order();

          // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
          foreach ($wcmeliOrder->normalizeOrderData->productItems as $key => $value) {
            $order->add_product( wc_get_product($value['product_id']), $value['quantity'],['subtotal'=>$value['total'],'total'=>$value['total']]); // This is an existing SIMPLE product
          }
          $order->set_address( $billing, 'billing' );
          $order->set_address( $shipping, 'shipping' );
          $order->update_meta_data( 'orderId', json_encode($order_id) );
          $order->calculate_totals();
          $order->update_status("processing", 'WcSa2Meli: ', TRUE);
          // var_dump($order);
          // exit;

          $orderLabelAttributes['label']['id'] = $order_id;
          $orderLabelAttributes['label']['shipment_ids'] = $wcmeliOrder->normalizeCustomerData->shippingId;
          $orderLabelAttributes['token'] = $wcmeliProduct->accessToken;

          $productData = $wcmeliProduct->updatewcsa2meli(json_encode($orderLabelAttributes),base64_decode($return[0]),base64_decode($return[1]));

          $content = file_get_contents('https://sa2.com.br/wcsa2meli/wcsa2meli.php?query='.$productData);

          if(strlen($content) > 500) $content = (array)json_decode($content,true);

          $dir_label = __DIR__. '/etiquetas/'.$order_id.'.pdf';

          if(isset($content['label']['MessageError'])) $dir_label = null;
          else file_put_contents($dir_label,$content);

        // exit('label');

          $nome = $wcmeliOrder->normalizeCustomerData->firstName.' '.$wcmeliOrder->normalizeCustomerData->lastName ;
          $error_handling = new log("Novo Pedido Mercado Livre", "Numero do Pedido: ".json_encode($order_id), "Comprador: $nome", "nova compra");
          $error_handling->mensagem_email = "Nova compra Mercado Livre";
          $error_handling->log_etiqueta = $dir_label;
          $error_handling->email_novacompra = true;
          $error_handling->log_email = true;
          $error_handling->send_log_email();
          $error_handling->execute();

          $log = new WC_Logger();
          $log_entry .= __('Novo Pedido Nº '. json_encode($order_id) .' inserido com sucesso', true );
          $log->add( 'wcmeli', $log_entry );

          $fluxOrder->addOrderStore($order_id);

      } catch(Exception $exception) {
        $error = new error_handling("WCML: Erro ao criar pedido no WooCommerce", (string)$exception->getMessage(), "Pedido: ". $orderData['order']->order_id, "Erro pedido");
        $error->send_error_email();
        $error->execute();
        echo '<br>'.$exception->getMessage();
      }
    } else echo "Pedido já inserido no WooCommerce<br>";
  $fluxOrder->add_item($order_id);
}

}
 ?>
